package Introduction.Exceptions;

public class Exc1 {
    /*
    Zad 4: Utwórz metodę, która przyjmuje dwa parametry - liczbę ( int ) oraz napis
( String ). Metoda ma wyświetlić podany napis, przekazaną liczbę razy. W
przypadku gdy liczba wystąpień będzie mniejsza bądź równa zero pownien
zostać rzucony wyjątek IndexOutOfBoundsException , a w przypadku gdy
napis będzie null -em pownien zostać rzucony wyjątek
IllegalArgumentException
     */
    public static void main(String[] args) {
        try {
            wypisz(10, "hello");
        } catch (IllegalArgumentException e){
            System.out.println("Brak napisu");
        } catch (IndexOutOfBoundsException e){
            System.out.println("N< 0!!");
        }
    }
    public static void wypisz (int n, String s){

        if(s == null){
            throw new IllegalArgumentException();
        }
        if (n <= 0) {
            throw new IndexOutOfBoundsException();

        }
        for(int i =0; i < n; i++);
    }
}
