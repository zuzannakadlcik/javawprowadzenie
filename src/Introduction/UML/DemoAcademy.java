package Introduction.UML;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DemoAcademy {

    public static void main(String[] args) throws IOException {

        /*Academy a1 = new Academy("Politechnika Warszawska");
        Subject sub1 = new Subject("Mechatronika");

        OutputStream out = Files.newOutputStream(Paths.get("C:\\Users\\zuzan\\IdeaProjects\\JavaWwa2018\\hello2.txt"));
        Writer out1 = new OutputStreamWriter(out);
        PrintWriter printWriter = new PrintWriter(out1);
        {
            for (int i = 0; i <= 100; i++) {
                printWriter.println(i);
            }
            out1.write("");
            out1.close();
        }


        try (OutputStream outb = Files.newOutputStream(Paths.get("C:\\Users\\zuzan\\IdeaProjects\\JavaWwa2018\\hello3.txt"));
             DataOutputStream out2 = new DataOutputStream(outb)) {
            out2.writeInt(15);
            out2.writeInt(11);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (InputStream inputStream = Files.newInputStream(Paths.get("hello3.txt"));
             DataInputStream dataInputStream = new DataInputStream(inputStream)) {
            System.out.println(dataInputStream.readInt());
            System.out.println(dataInputStream.readInt());
        } catch (IOException e) {
            e.printStackTrace();
        }
        */


        try (OutputStream outb = Files.newOutputStream(Paths.get("C:\\Users\\zuzan\\IdeaProjects\\JavaWwa2018\\hello4.txt"));
             DataOutputStream out4 = new DataOutputStream(outb)) {
            out4.writeBytes("Hello");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (RandomAccessFile randomAccessFile = new RandomAccessFile("hello3.txt", "rw")){
            randomAccessFile.skipBytes(4);
            randomAccessFile.writeInt(19);
        } catch(IOException e){
            e.printStackTrace();
        }

        try (InputStream inputStream = Files.newInputStream(Paths.get("hello3.txt"));
             DataInputStream dataInputStream = new DataInputStream(inputStream)) {
            System.out.println(dataInputStream.readInt());
            System.out.println(dataInputStream.readInt());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
