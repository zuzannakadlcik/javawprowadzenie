package Introduction.UML;

public class Lecturer extends Person {
    public Lecturer(String name, int age){
        super(name, age);
    }

    @Override
    public void printlntroduce() {
        System.out.println("Nazywam się" + this.name + " i mam " + " lat.");
    }

    @Override
    public String toString() {
        return "Lecturer{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
