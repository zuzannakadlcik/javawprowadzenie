package Introduction.UML;

import java.util.ArrayList;


public class Subject {
    private final String name;

    public Subject(String name) {
        this.name = name;
    }

    public void addStudent(Student s){
        students.add(s);

    }
    public void addLecturer(Lecturer  l){
        lecturers.add(l);
    }



    private ArrayList <Lecturer> lecturers = new ArrayList<>();


    private  ArrayList <Student> students = new ArrayList<>();

    public ArrayList<Lecturer> getLecturers() {
        return lecturers;
    }

    public void setLecturers(ArrayList<Lecturer> lecturers) {
        this.lecturers = lecturers;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "name='" + name + '\'' +
                ", lecturers=" + lecturers +
                ", students=" + students +
                '}';


    }
}

