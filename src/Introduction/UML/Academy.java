package Introduction.UML;

import java.util.ArrayList;

public class Academy {
    private final String name;

    private final ArrayList <Subject> subjects  = new ArrayList<>();

    public Academy(String name) {
        this.name = name;
    }
    public void addSubject(Subject s){
        subjects.add(s);
    }

    @Override
    public String toString() {
        return "Academy{" +
                "name='" + name + '\'' +
                ", subjects=" + subjects +
                '}';
    }
}
