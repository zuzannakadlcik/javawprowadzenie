package Introduction.UML;

public class Student extends Person {
    private final int albumNr;

    public Student(String name, int age, int albumNr){
        super(name, age);
        this.albumNr = albumNr;

    }

    @Override
    public void printlntroduce() {
        System.out.println("Nazywam się" + this.name + " i mam " + " lat. Numer mojego albumu to " +albumNr);
    }

    @Override
    public String toString() {
        return "Student{" +
                "albumNr=" + albumNr +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
