package JavaWwa2018.OOP.Collections;

public class Student implements Comparable <Student>{
    private String name;
    private String surname;
    private int year;
    private double avrg;

    public Student(String name, String surname, int year, double avrg) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.avrg = avrg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getAvrg() {
        return avrg;
    }

    public void setAvrg(double avrg) {
        this.avrg = avrg;
    }

    public int compareTo(Student s){
        return this.getSurname().compareToIgnoreCase(s.getSurname());

    }

}
