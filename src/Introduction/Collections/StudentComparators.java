package JavaWwa2018.OOP.Collections;

import java.util.Comparator;

public class StudentComparators {

    public static class AvgMarkAscComp implements Comparator<Student> {
        @Override
        public int compare(Student s1, Student s2) {
            return Double.compare(s2.getAvrg(),s1.getAvrg());
        }
    }

    public static class SurnameCheck implements Comparator<Student> {
        @Override
        public int compare(Student s1, Student s2) {
            return 0;
        }
    }

    public static class AscYear implements Comparator<Student> {
        @Override
        public int compare(Student s1, Student s2) {
            return 0;
        }
    }

}
