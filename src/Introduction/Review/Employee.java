package JavaWwa2018.OOP.Review;

public class Employee {

    /*
    Zad 13: Utwórz klasę Pracownik posiadającą
pola:
•    imie
•    drugie imie
•    nazwisko
•    wiek
•    płeć ( true = kobieta, false = mężczyzna)
•    pensję
•    adres (z poprzedniego zadania) (obiekt tej klasy)
gettery & settery
następujące kostruktory:
•    bezparametrowy
•    2-parametrowy (przyjmujący imię, nazwisko i płeć)
metody zwracające:
•    imie, drugie imie (jeśli istnieje!) i nazwisko (jako jeden string)
•    płec w formie tekstowej (tzn. Kobieta lub Mężczyzna )
•    liczbę lat pozostałych do emerytury (dla Kobiet 60, dla Mężczyzn 65)
     */
    private String name;
    private String secondName;
    private String surname;
    private int age;
    private boolean gender;
    private double sallary;
    private Address address = new Address();

    public Employee() {
    }

    public Employee(String name, String surname, boolean gender) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
    }

    public Employee(String name, String secondName, String surname, int age, boolean gender, double sallary, Address address) {
        this.name = name;
        this.secondName = secondName;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
        this.sallary = sallary;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public double getSallary() {
        return sallary;
    }

    public void setSallary(double sallary) {
        this.sallary = sallary;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String personalData() {
        if (secondName != null) {
            return name + " " + secondName + " " + secondName;
        }
        return name + " " + surname;
    }

    public String gender (){
        return (gender) ? "Female" : "Male";
    }


}
