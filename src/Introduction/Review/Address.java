package JavaWwa2018.OOP.Review;

public class Address {

    /*
    Zad 7: Utwórz klasę Address ( Adres ) posiadającą
Pola:
•    ulica
•    numer domu
•    numer mieszkania
•    kod pocztowy
•    miasto
Konstruktory:
•    bezparametrowy
•    3-parametrowy (przyjmujący pierwsze trzy pola)
•    5-parametrowy (przyjmujący wszystkie pola)
metody zwracające:
•    pełen adres
•    ulicę (wraz z numerami)
     */

    private String street;
    private int houseNumber;
    private int flatNumber;
    private String zipCode;
    private String town;

    public Address(){
        street = "Niska";
        houseNumber = 5;
        flatNumber = 3;
        zipCode  = "00-301";
        town = "Warsaw";
    }

    public Address(String street, int houseNumber, int flatNumber) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
    }

    public Address(String street, int houseNumber, int flatNumber, String zipCode, String town) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
        this.zipCode = zipCode;
        this.town = town;
    }

    public String getStreet() {
        return street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getTown() {
        return town;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getAdress(){

        return  this.street +  this.houseNumber + this.flatNumber + this.zipCode +this.town;
    }
}
