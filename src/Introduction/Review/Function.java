package JavaWwa2018.OOP.Review;

public class Function {

    /*
Zad 9: Utwórz klasę Rownanie służącą do policzenia rownania a^2 + b^3 + c^4 .
     Klasa powinna zawierać:
     pola:
•    a , b , c
kostruktory:
•    bezparametrowy
•    3-parametrowy
metody:
•    liczaca wartosc równania
•    przyjmującą liczbę value następnie zwracająca informację( boolean )
czy wartość równania przekroczyła podaną liczbą (jako parametr)
     */

    private double a;
    private double b;
    private double c;

    public Function(){
    }

    public Function(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void sum(){
        double sum = Math.pow(a,2) + Math.pow(b,3) + Math.pow(c,4);
        System.out.println(sum);
    }





}
