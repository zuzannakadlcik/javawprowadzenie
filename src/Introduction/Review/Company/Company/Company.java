package JavaWwa2018.OOP.Review.Company.Company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company
{
    private List<Worker> list = new ArrayList<Worker>();

    public Company(List<Worker> list) {
        this.list = list;
    }

    @Override
    public String toString()
    {
        String output = new String();
        for (Worker i : list)
        {
            output = output + "\n\n" + i;
        }
        return output;
    }

    public Worker highestSalary ()
    {
        return Collections.max(list); // tutaj używamy standardowego porównania dla klasy worker, czyli według pensji
    }

    public Worker oldest ()
    {
        AgeComparator ac = new AgeComparator(); //tutaj używamy dodatkowego porównania,
        // dla którego musimy stworzyć nowy obiek Comparator, ponieważ nie jest to standardowe porównanie
        return Collections.max(list, ac);
    }

    public int howManyOlder (int n)
    {
        int amount = 0;
        for (Worker i :list)
        {
            if (i.getAge() > n) amount++;
        }
        return amount;
    }

    public void percentRise (double amount)
    {
        for (Worker i : list)
        {
            i.setSalary(i.getSalary()+i.getSalary()*amount);
        }
    }

    public void amountRise (double amount)
    {
        for (Worker i : list)
        {
            double j = i.getSalary()+ amount;
            i.setSalary(i.getSalary() + amount);
        }
    }
}
