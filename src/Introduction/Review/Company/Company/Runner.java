package JavaWwa2018.OOP.Review.Company.Company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        Address ad = new Address ("Krasińskiego", 22, 54, "01-593", "Warszawa");
        Address ad2 = new Address ("Kopernika", 30, 1, "01-533", "Warszawa");

        Worker worker1 = new Worker("Krzysztof", "Woźniak", 45, false, 3200, ad);
        Worker worker2 = new Worker("Marcin", "Krychowiak", 35, false, 2800, ad);
        Worker worker3 = new Worker("Dominika", "Łęcka", 32, false, 2800, ad2);
        Worker worker4 = new Worker("Paulina", "Wysocka", 38, false, 3300, ad2);

        Worker [] arr = {worker1, worker2, worker3, worker4};
        List<Worker> list = new ArrayList<Worker>(Arrays.asList(arr));

        Company company = new Company(list);

        System.out.println("Lista Wszystkich pracowników:\n");
        System.out.println(company);

        System.out.println("\nNajwyższa pensja:\n");
        System.out.println(company.highestSalary());

        System.out.println("\nNajstarszy pracownik:\n");
        System.out.println(company.oldest());

        System.out.println("\nLiczba pracowników starszych niż 35 lat\n");
        System.out.println(company.howManyOlder(35));

        System.out.println("\nZwiększamy pensję pracowników o 15%\n");
        company.percentRise(0.15);
        System.out.println(company);

        System.out.println("\nZwiększamy pensję pracowników o 200\n");
        company.amountRise(200);
        System.out.println(company);
    }
}
