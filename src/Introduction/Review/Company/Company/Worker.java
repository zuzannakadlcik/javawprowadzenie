package JavaWwa2018.OOP.Review.Company.Company;



public class Worker implements Comparable <Worker>
{
    private String firstname;
    private String secondName;
    private String lastName;
    private int age;
    private boolean sex;
    private double salary;
    private Address address;

    public Worker() {
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    private String sexToString () // tę funkcję stosujemy tylko wewnątrz klasy przy metodzie toString
    {
        if (sex) return "Kobieta";
        return "Mężczyzna"; //Czemu to tak działa??
    }

    //konstruktor przyjmujący wszystkie parametry dla ułatwienia
    public Worker(String firstname, String lastName, int age, boolean sex, float salary, Address address) {
        this.firstname = firstname;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
        this.salary = salary;
        this.address = address;
    }

    @Override
    public String toString()
    {
        return firstname + " " + lastName + '\n' + age + " lat, " + sexToString() + "\nWynagrodzenie: " + salary + "\nAdres: " + address;
    }

    @Override
    public int compareTo(Worker other)
    {
        return (int)(salary - other.getSalary());
    }
}
