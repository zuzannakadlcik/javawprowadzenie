package JavaWwa2018.OOP.Review.Company.Company;

public class Address
{
    private String ulica;
    private  int numerDomu;
    private  int numerMieszkania;
    private String kodPocztowy;
    private String miasto;

    public Address() {}

    public Address(String ulica, int numerDomu, int numerMieszkania)
    {
        this.ulica = ulica;
        this.numerDomu = numerDomu;
        this.numerMieszkania = numerMieszkania;
    }

    public Address(String ulica, int numerDomu, int numerMieszkania, String kodPocztowy, String miasto)
    {
        this.ulica = ulica;
        this.numerDomu = numerDomu;
        this.numerMieszkania = numerMieszkania;
        this.kodPocztowy = kodPocztowy;
        this.miasto = miasto;
    }

    public String ulica ()
    {
        return ulica + " " + numerDomu + " m. " + numerMieszkania;
    }

    public String pelnyAdres ()
    {
        return ulica() + "\n" + kodPocztowy + " " + miasto;
    }

    @Override
    public String toString()
    {
        return pelnyAdres();
    }
}
