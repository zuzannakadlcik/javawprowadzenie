package JavaWwa2018.OOP.Review;

public class Alist {
    /*
    Zad 10: Utwórz klasę Tablica służącą do obsługi tablicy jednowymiarowej z
możliwością dynamicznej zmiany rozmiaru.
konstruktor:
•    jednoparametrowy (przyjmujący początkowy rozmiar tablicy)
pole:
•    będące tablicą, ale z modyfikatorem private
metody umożliwiające:
•    Wstawienie elementu na koniec
•    Zwrócenie rozmiar tablicy
•    Wyświetlenie tablicy
•    Rozszerzenie tablicy o wybraną liczbę pozycji na końcu (domyślnie o 1 )
•    Wstawienie elementu na początek
•    Wstawienie elementu pod wybranym indeksem
•    Usunięcie elementu pod wybranym indeksem
•    Zwrócenie największego elementu w tablicy
     */
}
