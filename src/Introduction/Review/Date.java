package JavaWwa2018.OOP.Review;

public class Date {
    /*
    Zad 8: Utwórz klasę Date ( Data ) posiadającą
pola:
•    dzień
•    miesiąc
•    rok
konstruktory:
•    bezparametrowy
•    3-parametrowy (przyjmujący wszystkie parametry)
metody umożliwiające:
•    ustawienie dnia (wraz ze sprawdzaniem poprawności) - zwracająca typ boolean
•    ustawienie miesiąca (wraz ze sprawdzaniem poprawności) - zwracająca typ boolean
•    ustawienie roku (wraz ze sprawdzaniem poprawności) - zwracająca typ boolean
•    wyświetlenie daty w formacie dzien-miesiac-rok (opcjonalnie z możliwością ustawienia separatora)
•    wyświetlenie daty w formacie rok-miesiac-dzien (opcjonalnie z możliwością ustawienia separatora)
•    przesunięcie daty o wybraną liczbę dni
     */

    private int day;
    private int month;
    private int year;


    public Date() {
    }
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public boolean setDay(int day) {
        if(this.day >= 1 && this.day<=30){
        this.day = day;
        return true;}
            return false;
        }

    public boolean setMonth(int month) {
        if(this.month >= 1 && this.month<=12){
        this.month = month;
        return true;}
            return false;
        }

    public boolean setYear(int year) {
        if(this.year > 1){
        this.year = year;
        return true;}
            return false;
    }

    public int getDay() {
        return day;
    }
    public int getMonth() {
        return month;
    }
    public int getYear() {
        return year;
    }

    public void dayMonthYear() {
        System.out.println("Date{" +
                day +
                "-" + month +
                "-" + year);
    }

    public void yearMonthDay(){
        System.out.println( "Date" + year +"-"+ month +"-"+ day);
    }

    public void moveDate(int days){

        int moveYears = days/360;
        int moveMonths = (days/30)%12;
        int moveDays = days%30;



        day +=moveDays;
        month += day/30;
        day = day%30;
        month += moveMonths;
        year += month/12;
        month = month%12;
        year += moveYears;

    }
}
