package Introduction;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;

public class Zad1Zemsta {

    public static void main(String[] args) throws IOException {
        int wordMoreThan12 = 0;
        FileChannel fileChannel = FileChannel.open(Paths.get("zemsta.txt"));
        ByteBuffer byteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0,fileChannel.size());
        byte [] wyraz = new byte [50];

        int charsCounter = 0;
        while (byteBuffer.remaining()>0){
            byte readedByte = byteBuffer.get();
            char readedChar = (char) readedByte;
            System.out.println(readedChar);

            if(Character.isWhitespace(readedByte)){
                if(charsCounter > 12){
                    wordMoreThan12++;
                }
                charsCounter = 0;
            } else {
                wyraz[charsCounter] = readedByte;
                charsCounter++;
            }
        }


    }
}
