package JavaWwa2018.OOP.Homework;

public class Ex3 {
    /*
    Zad. 3    //pętle, operatory
Kod ISBN jest to 10-cyfrowy kod, który jednoznacznie identyfikuje daną książkę.
Cyfra najbardziej na prawo jest tzw. sumą kontrolną,
i może być określona na podstawie pozostałych 9 cyfr z warunku:

d1 + 2d2 + 3d3 + ... + 10d10

, gdzie dx oznacza cyfrę x patrząc od prawej.
Ta suma powinna być wielkrotnością liczby 11.
Wartość kontrolna d1 może być jakąkolwiek liczbą pomiędzy 0 a 10 (konwencja ISBN oznacza 10 poprzez X).
Przykład: dla kodu ISBN 020131452(d1) suma
wg powyższego wzoru wynosi: d1 + 2*2 + 3*5 + 4*4 + 5*1 + 6*3 + 7*1 + 8*0 + 9*2 + 10*0.
Jedyną możliwą wartością kontrolną dla d1 może być tu 5,
ponieważ tylko podstawiając do wzoru 5 za d1 otrzymamy liczbę będącą wielokrotnością 11.
Twoim zadaniem jest napisanie programu, który przyjmuje ciąg 9 cyfr,
wyznacza sumę kontrolną (d1) i wyświetla pełen numer ISBN (10 cyfr z sumą kontrolną).
     */
}

