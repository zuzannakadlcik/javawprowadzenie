package JavaWwa2018.OOP.Homework.Interfaces;

public interface Movable {

    void moveUp();
    void moveDown();
    void moveRight();
    void moveLeft();


}
