package JavaWwa2018.OOP.Homework;

public class Ex5 {
}
/*
Zad. 5    //funkcje, pętle  ---> TRUDNE
Pamiętacie teleturniej z "zonkiem"? Licencja gry-show telewizyjnego "Let's Make a Deal" zakłada,
że przed graczem stawia się możliwość wyboru jednego spośród 3 drzwi.
Za jednym z drzwi jest nagroda, za 2 - nie.
Kiedy gracz wybierze drzwi, prowadzący otwiera jedno z pozostałych dwóch drzwi
(oczywiście nigdy nie to, które zawiera nagrodę!).
 Wtedy daje się graczowi wybór: może pozostać przy swoim pierwszym wyborze,
 lub zmienić go, wybierając pozostałe drzwi.
Zastosujemy statystykę i symulację, żeby sprawdzić, która ze strategii gry jest lepsza.
Zgodnie z intuicją oba drzwi powinny nieść taką samą szansę wygrania.
Napisz program, który przyjmuje wartość n, oznaczającą,
ile razy powinna być rozegrana gra, stosując jedną jak i
drugą strategię (zawsze zmiana wyboru lub nigdy zmiana wyboru).
Po przeprowadzonej symulacji wyświetl prawdopodobieństwo odniesienia sukcesu w jednej i drugiej strategii.
*/
