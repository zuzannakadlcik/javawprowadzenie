package JavaWwa2018.OOP.Homework;

public class Ex2 {
    /*
    Zad. 2    //switch
Napisz program, który dla podanej daty określi dzień tygodnia.
Do programu przekazujemy trzy argumenty:
d (dzień), m (miesiąc, gdzie 1 oznacza styczeń, 2 luty itd.), y (rok).
Skorzystaj z następujących wzorów:

y0 = y − (14 − m) / 12
x = y0 + y0/4 − y0/100 + y0/400
m0 = m + 12 × ((14 − m) / 12) − 2
d0 = (d + x + 31m0 / 12) mod 7

d0 odpowiada wyznaczonemu dniu tygodnia, gdzie 0 oznacza niedzielę, 1 poniedziałek itd.
Dla podanych wartości d, m, y wyświetl dzień tygodnia, korzystając z konstrukcji switch.
 */

    public static void main(String[] args) {

        int y = 2018;
        int m = 7;
        int d = 23;

        int y0 = y - (14 - m) / 12;
        int x = y0 + y0/4 - y0/100 + y0/400;
        int m0 = m + 12*((14- m) / 12)- 2;
        int d0 = (d + x + 31 * m0 / 12) % 7;

        int day = d0;
        String sth = new String();

        switch (day) {
            case 0:
                System.out.println("Poniedziałek");
                break;
            case 1:
                System.out.println("Wtorek");
                break;
            case 2:
                System.out.println("Sroda");
                break;
            case 3:
                System.out.println("Czwartek");
                break;
            case 4:
                System.out.println("Piatek");
                break;
            case 5:
                System.out.println("Sobota");
                break;
            case 6:
                System.out.println("Niedziela");
                break;
            default:
                System.out.println("Unknow data");
        }


    }
}
