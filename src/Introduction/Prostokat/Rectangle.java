package JavaWwa2018.OOP.Prostokat;

public class Rectangle {

    /*1) Utworzyc klase Rectangle
2) Ustawic domyslne wartosci pol tej klasy (jedna z dwoch poznanych metod)
3) Zaimplementowac metody obliczajace: calcArea, calcPerimiter
4) Utworzyc klase Runner z metoda main
5) W metodzie main klasy Runner utworzyc 3 zadane obiekty klasy Rectangle
6) Napisac w klasie Runner dodatkowa metode statyczna,
ktora dla przekazanego obiektu klasy
Rectangle wypisuje o nim informacje:
"Prostokąt o wymiarach sideA x sideB, koloru color. Jego obwod to ... a pole ... "
7) Wywolac metode z pkt 6 na obiektach z pkt 5 - w metodzie main klasy Runner
*/

   private double blokA;
   private double blokB;
   private String kolor;

   public Rectangle(){
       blokA = 10.0;
       blokB = 10.0;
       kolor = "red";
   }


    public Rectangle(double blokA, double blokB, String kolor) {
        this.blokA = blokA;
        this.blokB = blokB;
        this.kolor = kolor;
    }

    public double calcArea(){

        return blokA*blokB;

    }

    public double calcPerimiter(){

        return (2*blokA)+(2*blokB);

    }


    public double getBlokA() {
        return blokA;
    }

    public double getBlokB() {
        return blokB;
    }

    public String getKolor() {
        return kolor;
    }



}
