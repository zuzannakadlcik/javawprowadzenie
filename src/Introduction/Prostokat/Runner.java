package JavaWwa2018.OOP.Prostokat;

public class Runner {

    public static void main(String[] args) {

        Rectangle r1 = new Rectangle(2.0,3.0,"niebieski");
        r1.calcArea();
        r1.calcPerimiter();

        Rectangle r2 = new Rectangle(4.0,2.0,"zielony");
        r2.calcArea();
        r2.calcPerimiter();

        Rectangle r3 = new Rectangle(3.0,1.0,"czarny");
        r3.calcArea();
        r3.calcPerimiter();

    }
}


