package JavaWwa2018.OOP.OOP;

public class Podzielnosc {
    /*
    Zad 15: Utwórz klasę Podzielnosc przyjmujacą w konstruktorze liczbę do sprawdzenia.
     Liczba powinna być przechowywana w formie tablicy wartości typu int
     (czyli liczba 1234 ma zostać zamieniona na tablicę [1, 2, 3, 4] ).
By konwertować liczbę na wartości, możesz najpierw zamieniać liczbę na tablicę napisów
(druga linia), a następnie pętlą rzutować każdy ze znaków na liczbę (czyli napis "5" na liczbę 5 :disappointed:

int x = 1234567;
String[] tablicaNapisow = Integer.toString(x).split("");
int[] tablicaLiczb = new int[tablicaNapisow.length];
for (int i = 0; i < tablicaLiczb.length; i++) {
tablicaLiczb[i] = Integer.parseInt(tablicaNapisow[i]);
}
Klasa Powinna posiadać metody umożliwiające sprawdzenie podzielności w zakresie od 1 do 10; Czyli
powinna posiadać metody (zwracające boolean !):
•    podzielnaPrzez1()
•    podzielnaPrzez2()
•    podzielnaPrzez3()
•    . . .
•    podzielnaPrzez9()
•    podzielnaPrzez10()

Przetestuj działanie w nowej klasie. Na przykład uruchom pętlę od 10 do 40 i dla każdej liczby wyświetl podzielność w formie:
10 -> 1, 2, 5, 10
11 -> 1, 11
12 -> 1, 2, 3, 4, 6, 12
13 -> 1, 13
14 -> 1, 2, 7, 14
...
     */
}
