package JavaWwa2018.OOP.OOP.Shape;

public class Rectangle implements Shape {

    private double a,b;
    String color;


    public Rectangle(double a, double b, String color) {
        this.a = a;
        this.b = b;
        this.color = color;
    }

    //aby wygenerowac 'zaczatek' do implementacji metody
    //alt + insert -> implement methods
    @Override
    public double calcArea() {
        return a * b;
    }

    //alt + insert -> override methods zeby przeciazyc domyslna implementacje metody String
    @Override
    public String toString() {
        return "Jestem prostokątem o wymiarach " + a + "x" + b + ", koloru "
                + color +   //pole protected z klasy rodzica, dostep bezposredni
                " - moje pole powierzchni wynosi " + calcArea();
    }
}