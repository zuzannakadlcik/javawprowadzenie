package JavaWwa2018.OOP.OOP.Shape;

public interface Shape {

    //uzywamy modyfikatora protected, zeby miec bezposredni dostep do tego pola z poziomu klas pochodnych

    double calcArea(); //przynajmniej jedna metoda abstrakcyjna = klasa abstrakcyjna

}

