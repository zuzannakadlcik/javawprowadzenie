package JavaWwa2018.OOP.OOP.Shape;

public class Runner {

    public static void main(String[] args) {


        Rectangle r1 = new Rectangle(20, 15, "blue");
        Rectangle r2 = new Rectangle(12, 4, "red");
        Triangle t1 = new Triangle(15, 7.5, "yellow");
        Triangle t2 = new Triangle(22, 11, "pink");

        //mamy wspolna klase bazowa, wiec mozemy zrzutowac do tej klasy obiekty pochodne
        Shape[] shapes = {r1, r2, t1, t2};

        for(Shape s : shapes) {
            System.out.println(s);  //explicite wola metode toString()
        }

        //Shape s1 = new Shape();   //nie jest mozliwe utworzenie obiektu klasy abstrakcyjnej
        Shape s1 = t1;  //ale moge sobie pod referencje klasy rodzica zapisywac klase dziecka
    }
}