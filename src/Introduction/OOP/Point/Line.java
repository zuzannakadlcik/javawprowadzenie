package JavaWwa2018.OOP.OOP.Point;

public class Line {

   private Point begin;
   private Point end;

    public Line(int x1,int y1, int x2, int y2) {
        begin = new Point (x1, y1); // stwórz obiekt w konstruktorze
        end = new Point (x2, y2);
    }

    public Line(Point begin, Point end) {
        this.begin = begin;
        this.end = end;
    }

    public Point getBegin() {
        return begin;
    }

    public void setBegin(Point begin) {
        this.begin = begin;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }
    public int getBeginY(){
        return begin.getY();
    }
    public int getBeginX(){
        return begin.getX();
    }

    public int getEndY(){
        return end.getY();
    }

    public void setBeginX(int x){
        this.begin.setX(x);

    }

    public void setBeginY(int y){
        this.begin.setY(y);

    }
    public void setEndX(int x){
        this.end.setX(x);

    }
    public void setEndY(int y){
        this.end.setY(y);

    }

   public int [] getBeginXY(){
       return begin.getXY();
   }

   public int []getEndXY(){
        return end.getXY();
   }
    public void setBeginXY(int x, int y) {
        begin.setX(x);
        begin.setY(y);
    }

    public void setEndXY(int x, int y) {
        end.setX(x);
        end.setY(y);
    }

    @Override
    public String toString() {
        return "Odcinek: początek" + begin.toString() +
                "koniec" + end.toString();
    }

    public double getLength(){
        return begin.distance(end);
    }
}


