package JavaWwa2018.OOP.OOP.Point;

public class Point {
    private int x;
    private int y;

    public Point() {
        x = 0;
        y = 0;
    }
    //konstruktor przeciążony usuwa domyślny;
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" +
                +x +
                "," + y +
                ')';
    }
    public void setXY(int x, int y) {
        setX(x);
        setY(y);
    }

    public int[] getXY() {
        int[] tab = {x, y};
        return tab;
    }
    public double distance (int x, int y){
        return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y,2));
}
    public double distance (){
        return distance (0,0);

    }

    public static void printDistance (Point p1, Point p2){
        System.out.println( "Odległość między punktami  " + p1+ "od punktu " + p2 + "wynosi" + p1.distance(p2));
    }
    public double distance (Point p){
        return distance(p.getX(), p.getY());
    }

}


