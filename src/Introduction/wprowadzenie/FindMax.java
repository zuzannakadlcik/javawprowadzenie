package JavaWwa2018.OOP.wprowadzenie;

public class FindMax {

    public static int max3(int a, int b, int c) {
        //rzutujemy argumenty z typu int na dub, zeby uniknac bledu stack overflow,
        // zwiazanego z wywolaniem rekurencyjnym (funkcja wywolujaca te sama funkcje
        return (int)max3((double) a, (double)b, (double)c);

    }

    public static double max3(double a, double b, double c) {

        double max = a; //stworz wartosc lokalna i przypisz jedna ze zmiennych w metodzie

        if (max < b) {
            max = b;
        }
        if (max < c) {
            max = c;
            //konstruckja if sprawdza co jest wieksze, poprzez porownanie nowej zmiennej max
        }

        return max;
    }

    public static void main(String[] args) {

        int[] tab = new int[9];

        //wypełniamy tabele losowymi liczbami z przedzialu <0,100>
        System.out.print("Tabela:  [:");
        for (int i = 0; i < tab.length; i++) {
            tab[i] = (int) (Math.random() * 100);
            System.out.print(tab[i] + " , ");
        }

        System.out.print("]");

        /* wtej petli chce wylosowac max liczby sposrod:
        1) tab[0], tab[1], tab[2] -> max1
        2) tab[3], tab[4], tab[5] -> max2
        3) tab[6], tab[7], tab[8] -> max3
         */
        int[] maxtab = new int[3];
        for (int i = 0; i < tab.length; i += 3) {
            maxtab[i/3] = max3(tab[i], tab[i + 1], tab[i + 2]);
            System.out.println("Maksymalna wartosc z grupy " +(i/3+1)+ " wynosi "+ maxtab[i/3]);
        }
        // kolejny krok to +3

        System.out.println("MAXz calej tabeli to");


    }


}
