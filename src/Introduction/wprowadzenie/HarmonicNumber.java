package pl.sda.javawwa10;

public class HarmonicNumber {

    /*public static void main(String[] args) {
        //np. obliczenie liczby harmonicznej dla 5:
        //suma harmoniczna = 1 + 1/2 + 1/3 + 1/4 + 1/5

        //suma jest liczba zmiennoprzecinkowa - musze przechowywac ja w odpowiednim typie danych
        double sum = 0.0;
        int harmonicFromNumber = 5;

        //chce zrobic tyle iteracji, zeby w mianowniku ostatniego skladnika bylo 5 (czyli 1/5)
        //wprowadzam liczbe ktora kontroluje ilosc obrotow (iterator)
        int iteration = 1;
        while(iteration <= harmonicFromNumber) {    //krec sie az iteracja bedzie rowna 5 (i wtedey tez sie zakrec)
            sum = sum + 1.0/iteration;    //jesli bylby int 1/2 -> 0.5 -> 0
            System.out.println("Wartosc po iteracji " + iteration + " wynosi " + sum);
            iteration++;    //++ oznacza zwiekszenie o 1 czyli: i = i + 1
        }

        System.out.println("Liczba harmoniczna dla " + harmonicFromNumber + " wynosi " + sum);
    }*/

    //alternatywne rozwiazanie
    public static void main(String[] args) {
        double sum = 0.0;
        int harmonicNumber = 5;

        //sum = 1/5 + 1/4 + 1/3 ... + 1
        while(harmonicNumber > 0) {
            sum += 1.0/harmonicNumber;  //sum = sum + 1.0/harmonicNumber
            harmonicNumber--;   //i = i - 1
        }

        System.out.println("Liczba harmoniczna wynosi " + sum);
    }

}
