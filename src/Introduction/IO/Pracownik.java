package Introduction.IO;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Pracownik implements Serializable {
    private int ID;
    private int age;

    public Pracownik( int ID, int age){
        this.ID = ID;
        this.age = age;
    }


    @Override
    public String toString() {
        return "Pracownik{" +
                "ID=" + ID +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {
        Pracownik p1 = new Pracownik(23444,23);
        ObjectOutputStream out;


            try {
                out = new ObjectOutputStream(Files.newOutputStream(Paths.get("pracownik.txt")));
                out.writeObject(p1);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        try( ObjectInputStream in = new ObjectInputStream(Files.newInputStream(Paths.get("pracownik.txt")))) {
                Pracownik p =  (Pracownik) in.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e1){

        }





    }
}
